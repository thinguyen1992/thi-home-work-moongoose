const User = require('../models/user.model.js');
const url = require('url');
const https = require('https');
const fs = require('fs');
const dataGitHub = require('../dataaccess/user.dataaccess.js');


// Save FormData - User to MongoDB
exports.save = async(req, res) => {
    console.log(req.file)
    console.log("Username: " ,req.body);

    let data = await dataGitHub.getUserDataFromGithub(req.body.username);
    let imageURLFromData = req.file.path;
    console.log("imageURL: ", imageURLFromData)
    //let imageURL = fs.readFileSync(req.body.imageURL);

    // Create a Customer
    const user = new User({
        id: data.id,
        login: data.login,
        imgURL: imageURLFromData.replace(/resources/g, "")
    });
 
    // Save a Customer in the MongoDB
    user.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};
 
// Fetch all Users
exports.findAll = (req, res) =>  {
	console.log("Fetch all Users");
	
    User.find()
    .then(users => {
        res.send(users);
        console.log("Users",users)
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
};