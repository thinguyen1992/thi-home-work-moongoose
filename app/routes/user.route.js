var multer  = require('multer')
var crypto  = require('crypto')
var mime  = require('mime')
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'resources/uploads/')
	},
	filename: function (req, file, cb) {
	  crypto.pseudoRandomBytes(16, function (err, raw) {
		cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
	  });
	}
  });
var upload = multer({ storage: storage });

module.exports = function(app) {
 
	var express = require("express");
	var router = express.Router();
	
    const users = require('../controllers/user.controller.js');
	
	var path = __basedir + '/views/';
	
	router.use(function (req,res,next) {
		console.log("/" + req.method);
		next();
	});
	
	app.get('/', (req,res) => {
		res.sendFile(path + "index.html");
	});
 
    // Save a User to MongoDB
    app.post('/api/users/save',upload.single('image'), users.save);
 
    // Retrieve all Users
    app.get('/api/users/all', users.findAll);
	
	app.use("/",router);
 
	app.use("*", (req,res) => {
		res.sendFile(path + "404.html");
	});
}