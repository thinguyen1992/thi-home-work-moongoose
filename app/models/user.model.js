const mongoose = require('mongoose');
 
const UserSchema = mongoose.Schema({
    id: String,
    login: String,
    imgURL: String
});
 
module.exports = mongoose.model('User', UserSchema);