const https = require('https')
const url = require('url');

// get data from internet
function getUserDataFromGithub(user) {
    return new Promise((resolve, reject) => {
        let data = "";
 
        console.log("get user from github");

        let link = url.parse(`https://api.github.com/users/${user}`);
        let options = {
            hostname: link.hostname,
            path: link.path,
            method: "GET",
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
            }
        }
        let req = https.request(options, async(res) => {

            let resBody = "";
            try {
                data = require('./data.json').toString();
            } catch (err) {
                console.log(`data error ${err.message}`)
            }

            res.setEncoding("UTF-8");

            res.once('data', function() {
                console.log('start retrieving data...')
            })

            res.on('data', function(chunk) {
                resBody += chunk;
            })

            res.on('end', () => {
                console.log('retrieving data finished.')
                resolve(JSON.parse(resBody.toString()));
                console.log(resBody);
            })
        })

        req.on("error", function(err) {
            console.log(`request error ${err.message}`)
        })
        req.end();

    })
}

exports.getUserDataFromGithub = getUserDataFromGithub;