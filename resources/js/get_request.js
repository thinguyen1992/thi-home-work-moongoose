$( document ).ready(function() {
	
	// GET REQUEST
	$("#allUsers").click(function(event){
		event.preventDefault();
		ajaxGet();
	});
	
	// DO GET
	function ajaxGet(){
		$.ajax({
			type : "GET",
			url : "/api/users/all",
			success: function(result){
				$('#getResultDiv .list-group').empty();
				$.each(result, function(i, user){
					$('#gitHubTable').append("<tr><td>" + user.id + "</td>" + "<td> "+user.login+"</td>" + "<td><img src='"+user.imgURL+"' width ='70' height ='70'/>"+"</td></tr>")
				});
				console.log("Success: ", result);
			},
			error : function(e) {
				$("#getResultDiv").html("<strong>Error</strong>");
				console.log("ERROR: ", e);
			}
		});	
	}
})