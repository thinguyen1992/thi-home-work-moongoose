$( document ).ready(function() {
	
	// SUBMIT FORM
    $("#userForm").submit(function(event) {
		// Prevent the form from submitting via the browser.
		event.preventDefault();
		ajaxPost();
	});
    
    
    function ajaxPost(){

		var fd = new FormData();    
		fd.append( 'image', document.getElementById("imageURL").files[0] );
		fd.append('username',$("#username").val());
    	
    	// PREPARE FORM DATA
    	
    	// DO POST
    	$.ajax({
			type : "POST",
			url : window.location + "api/users/save",
			data : fd,
			processData: false,
  			contentType: false,
			success : function(user) {
				$("#postResultDiv").html("<p>" + 
					"Post Successfully! <br>" +
					"--> id : " + user.id + " loginname: " + user.login + "</p>");
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});
    	
    	// Reset FormData after Posting
    	resetData();
 
    }
    
    function resetData(){
		$("#username").val("");
		$("#imageURL").val("");
    }
})